package main

import "fmt"

type Base struct{}

type Voodoo struct {
	Base
}

func main() {
	v := new(Voodoo)
	v.Magic()
	v.MoreMagic()
}

func (Base) Magic() { fmt.Print("base magic") }

func (self Base) MoreMagic() {
	self.Magic()
	self.Magic()
}

func (Voodoo) Magic() { fmt.Println("voodoo magic") }
