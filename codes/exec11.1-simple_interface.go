package main

import "fmt"

type Simpler interface {
	Get() int
	Set(int)
}

type Simple struct {
	num int
}

func main() {
	s := Simple{10}
	sI := s
	fmt.Printf("s.num value is: %d\n", sI.Get())
	sI.Set(100)
	fmt.Printf("s.num value is after change: %d\n", sI.Get())
}

func (s *Simple) Set(i int) {
	s.num = i
}

func (s Simple) Get() int {
	return s.num
}
