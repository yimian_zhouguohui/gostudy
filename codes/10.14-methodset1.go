package main

import "fmt"

type list []int

func main() {
	// A bare value
	var lst List
	lst.Append(1)
	fmt.Printf("%v (len: %d) \n", lst, lst.len())

	// A pointer value
	plst := new(List)
	plst.Append(2)
	fmt.Printf("%v (len: %d)\n", plst, lst.len())
}

func (l List) Len() int        { return len(l) }
func (l *List) Append(val int) { *l = append(*l, val) }
