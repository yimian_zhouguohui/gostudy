package main

import (
	"fmt"

	"./even"
)

func main() {
	for i := 0; i <= 100; i++ {
		fmt.Printf("Ts the integer %d even? %v\n", i, even.Even(i))
	}
}
