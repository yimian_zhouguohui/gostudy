package main

import "fmt"

func main() {
	for i := 0; i < 5; i++ {
		fmt.Printf("factorial(%d) == %d\n", i, factorial(i))
	}
}

func factorial(num int) (ret int) {
	if num == 0 {
		ret = 1
	} else {
		ret = num * factorial(num-1)
	}
	return
}
