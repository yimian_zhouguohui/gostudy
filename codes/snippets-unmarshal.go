package main

import (
	"encoding/json"
)

type Value struct {
	Name string `json:"name"`
	Addr string `json:"addr"`
}

type Response struct {
	OK     bool   `json:"ok"`
	Data   Urls   `json:"data"`
	Reason string `json:"reason"`
}

type Urls struct {
	ShortUrl  string `json:"short_url"`
	ShortHash string `json:"short_hash"`
	LongUrl   string `json:"long_url"`
	UrlMd5    string `json:"url_md5"`
}

func main() {
	v1 := Value{"tony", "Beijing"}
	v2 := Value{"jack", "Shenzhen"}
	v3 := Value{"lucy", "Chongqing"}

	values := []Value{v1, v2, v3}
	resultStr, err := json.Marshal(values)
	if err != nil {
		println(err.Error())
	}
	println(string(resultStr))

	jsonStr := `[{"name":"tony","addr":"Beijing"},{"name":"jack","addr":"Shenzhen"},{"name":"lucy","addr":"Chongqing"}]`
	var newValues []*Value
	json.Unmarshal([]byte(jsonStr), &newValues)
	println(newValues[0].Name)
	println(newValues[0].Addr)
	println(newValues[2].Name)
	println(newValues[2].Addr)
	println(len(newValues))

	var res Response
	jsonStr = `{"ok":true,"data":{"short_url":"http://www.domain.com/10WO0","short_hash":"10WO0","long_url":"http://www.baidu.com/","url_md5":"f03f5717616221de41881be555473a02"},"reason":"null"}`
	err = json.Unmarshal([]byte(jsonStr), &res)
	println(res.Data.ShortHash)
}
