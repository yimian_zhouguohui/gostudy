package main

import "fmt"

type Camera struct{}

type Phone struct{}

type CameraPhone struct {
	Camera
	Phone
}

func main() {
	cp := new(CameraPhone)
	fmt.Println("Our new CameraPhone exhibits multiple behaviors...")
	fmt.Println("As a camera:", cp.TakeAPicture())
	fmt.Println("As a phone:", cp.Call())
}

func (c *Camera) TakeAPicture() string {
	return "Click"
}

func (p *Phone) Call() string {
	return "Ring Ring .."
}
