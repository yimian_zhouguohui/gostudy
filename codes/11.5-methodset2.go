package main

import "fmt"

type List []int

type Appender interface {
	Append(int)
}

type Lener interface {
	Len() int
}

func main() {
	var lst List
	if LongEnough(lst) {
		fmt.Printf("- lst is long enough")
	}
	plst := new(List)
	CountInto(plst, 1, 10)
	if LongEnough(plst) {
		fmt.Printf("- plst is long enough")
	}
}

func (l List) Len() int { return len(l) }

func (l *List) Append(val int) { *l = append(*l, val) }

func CountInto(a Appender, start, end int) {
	for i := start; i <= end; i++ {
		a.Append(i)
	}
}

func LongEnough(l Lener) bool {
	return l.Len()*10 > 42
}
