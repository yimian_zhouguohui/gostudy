package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 4}
	LinePrint(arr...)
	LinePrint(4, 3, 2, 1)
}

func LinePrint(a ...int) {
	for _, v := range a {
		fmt.Println(v)
	}
}
