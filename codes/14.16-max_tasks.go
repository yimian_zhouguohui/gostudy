package main

const (
	AvailableMemory         = 10 << 20
	AverageMemoryPerRequest = 10 << 10
	MAXREQS                 = AvailableMemory / AverageMemoryPerRequest
)

var sem = make(chan int, MAXREQS)

type Request struct {
	a, b   int
	replyc chan int
}

func main() {
	queue := make(chan *Request)
	go Server(queue)
}

func process(r *Request) {
	// Do something
}

func handle(r *Request) {
	process(r)
	// signal done: enable next request to start
	// by making 1 empty place in the buffer
	<-sem
}

func Server(queue chan *Request) {
	for {
		sem <- 1
		// blocks when channel is full
		// so wait here until there is capacity to process a request
		request := <-queue
		go handle(request)
	}
}
