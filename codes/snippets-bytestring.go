package main

import (
	"bytes"
	"fmt"
)

const ALPHA = "abcdefghijklmnopqrstuvwxyz"

func main() {
	var b bytes.Buffer
	for i, a := range ALPHA {
		if i%2 == 0 {
			b.WriteString(string(a))
		}
	}
	fmt.Println(b.String())
}
