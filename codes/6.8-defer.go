package main

import "fmt"

func main() {
	Func1()
}

func Func1() {
	fmt.Println("In Func1 at the top")
	defer Func2()
	fmt.Println("In Func1 at the bottom")
}

func Func2() {
	fmt.Println("Func2: deferred until the end of the calling function.")
}
