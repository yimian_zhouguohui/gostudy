package main

import (
	"encoding/json"
	"fmt"
)

type Value struct {
	Name string `json:"name"`
	Addr string `json:"address"`
}

func main() {
	v1 := Value{"tony", "Beijing, Douban"}
	v2 := Value{"jack", "Guangdong, Shenzhen"}
	var values []Value = []Value{v1, v2}
	js, _ := json.Marshal(values)
	fmt.Printf(string(js))
}
