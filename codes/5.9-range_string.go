package main

import "fmt"

func main() {
	str := "Go is a beautiful language!"
	for pos, char := range str {
		fmt.Println("characer on position %d is: %c", pos, char)
	}

	str2 := "Chinese: 中国话"
	for pos, char := range str2 {
		fmt.Println("Character %c starts at byte position %d", char, pos)
	}
	fmt.Println("index int(rune) run char bytes")
	for index, rune := range str2 {
		fmt.Printf("%-2d    %d    %U '%c' % x\n", index, rune, rune, rune,
			[]byte(string(rune)))
	}
}
