package main

import (
	"fmt"
	"net/http"

	"github.com/go-martini/martini"
)

func main() {
	m := martini.Classic()
	m.Post("/multi/", func(r *http.Request) {
		r.ParseForm()
		for _, value := range r.PostForm {
			fmt.Println(value[0])
		}
		fmt.Println(len(r.PostForm))
	})
	m.Get("/", func() string {
		return "hello"
	})

	m.Run()
}
