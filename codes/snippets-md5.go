package main

import (
	"crypto/md5"
	"fmt"
)

func main() {
	str1 := []byte("http://www.baidu.com/")
	str2 := []byte("test")
	h := md5.New()
	ret1 := fmt.Sprintf("%x", h.Sum(str1))
	ret2 := fmt.Sprintf("%x", h.Sum(str2))
	fmt.Println(ret1[:32])
	fmt.Println(ret2[:32])
}
