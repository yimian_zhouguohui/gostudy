package main

import "fmt"

type ByteSize float64
type BitFlag int

const (
	_           = iota // ignore first value by assigning to blank identifier
	KB ByteSize = 1 << (10 * iota)
	MB
	GB
	TB
	PB
	EB
	ZB
	YB
)
const (
	Active  BitFlag = 1 << iota // 1 << 0 == 1
	Send                        // 1 << 1 == 2
	Receive                     // 1 << 2 == 4
)

func main() {
	fmt.Println("KB", KB)
	fmt.Println("GB", GB)
	fmt.Println("PB", PB)
	fmt.Println("ZB", ZB)

	flag1 := Active | Send
	flag2 := Send | Receive
	fmt.Println("Active|Send:", flag1)
	fmt.Println("Send|Receive", flag2)
}
