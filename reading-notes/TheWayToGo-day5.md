## TheWayToGo-day5

今天把昨天第十二章的剩下的章节读完了，然后是第十三章和第十四章前两节的内容。第十二章剩下的内容也是一些和读写操作相关的内容，第十三章内容是 Go 的错误处理和测试，第十四章是 Go 中最为特色的内容， goroutine 和 channel。下面是要点总结：

### 第十二章：Reading and writing 3-12

本章主要还是讲读写方面的内容，剩下包括了读取命令行参数，读写文件等内容，下面是本章的要点：

1.`io.Copy(dst, src)` 参数为两个打开的文件，可以直接将 src 文件内容复制到 dst 中；

2.可以通过 `os.Args` 来获取执行时的命令行参数；

3.处理命令行参数可以考虑使用 flag 包，提供更多的功能；

4.在使用 `os.Open` 打开一个文件之后，可以在后面加上 `defer f.Close()` 使得在函数结束的时候关闭文件；

5.`encoding/json` 包可以用来处理 json 数据；

6.`json.Marshal` 可以将一个 struct 变量的可访问的字段及值转换为 json 形式的字符串数据；

7.`json.UnMarshal(data []byte, v interface{})`可以将 json 格式的字符串转换为一个 struct 变量，并且 json 的字段的值会传给与 struct 对应的字段，没有对应的值则会被忽略掉；

8.`gob` 包可以用来序列化和反序列化 Go 的数据包括自定义的 struct 变量数据；

### 第十三章：Error-handling and Testing

Go 没有异常的机制，也没有 try/catch ，当然也不能抛出和捕获异常，Go 的错误处理很简单，利用 Go 函数支持多值返回的特性在发生错误的时候返回一个 error 变量，没有错误则对应返回 nil。这种方式实际上很类似与 C 语言返回错误码的方式，有人喜欢也有人吐槽。此外，Go 语言还可以通过一种所谓 panic-and-recover 的模式来处理错误。下面是本章的要点：

1.通常 Go 中的函数返回两个值，一个是函数的返回值，另外一个为错误码(error)，如果没有错误则错误码为 nil；

2.Go 中有一个预定义的错误处理接口类型 `type error interface { Error() string }`；

3.`error.New("error string")` 定义一个错误；

4.可以用 `panic` 函数在运行时产生错误停止整个程序；

5.当 panic 发生在一个函数发生时，会立即停止当前的函数执行，然后保证所有的 defer 语句执行之后，控制权会转交到调用这个函数的函数，然后依次上升到整个栈的最顶层，然后执行 defer 语句，最后终止整个程序；

6.当 panic 发生的时候，可以使用 recover 函数来重新获得控制权，制止 panic 发生的时候的终止序列，然后重新恢复到正常的执行顺序，也就是说 panic 不会再往上逐层停止函数执行，而是在 recover 这里恢复到正常的执行顺序；

7.recover 函数只有在 defer 语句中调用才有用，可以获取 panic 传递的错误值，在正常执行中调用 recover 返回的是 nil，没有任何其他作用；

8.在写自己的包的时候，应该注意处理 panic ，总是用 recover 获取 panic 的错误值，不应该让 panic 跨越包；

9.通常可以 defer 一个匿名函数也就是闭包函数，然后在这个闭包中调用 recover 处理错误；

10.测试和性能测试是 Go 开发中很重要的一部分，可以用 testing 和 pprof 包进行；

### 第十四章：Goroutines and Channels 1-2

Go 语言很大的一个卖点就是，它对并发编程有很好的支持，而这个支持就是由 goroutine 和 channel 来实现的。goroutine 是 Go 语言中类似微线程的一种实现，可以轻易地生成大量的 goroutine 进行处理，而各个 goroutine 之间的切换也很快。channel 则是 Go 语言中用来作为 goroutine 之间的通信方式。下面是这章 1-2 节的要点：

1.Go 使用的是 CSP 模型，也是所谓的 message-passing 模型；

2.一个 goroutine 映射到一个或多个系统级别的线程来执行，也就是说一个 goroutine 有可能会被多个线程执行，这个是由 goroutine-scheduler 进行管理实现的；

3.goroutine 在同一个地址空间执行的，所以对共享内存的访问需要进行同步的处理，在 Go 中可以用 sync 提供的锁或者其他类似机制来进行多个 goroutine 的同步处理，但是这是不被建议的；

4.Go 语言中多个 goroutine 之间的同步处理是通过 channel 来进行的；

5.当一个 goroutine 被系统调用阻塞的时候(比如 IO 等待)，其他的 goroutine 会被执行；

6.goroutine 是很轻量级别的，比系统的线程要少更多的内存和资源使用，创建一个 goroutine 的代价很低；

7.一个 goroutine 的创建是通过在函数，或者方法或者匿名函数前面添加一个 `go` 关键字进行的；

8.main 函数也可以被视作为一个 goroutine；

9.当 main 函数返回的时候，整个程序会退出，这时候其他没有执行完毕的 goroutine 也不会等待其执行完毕；

10.各个 goroutine 之间是独立的执行单元，彼此之间并没有固定的执行顺序，所以各个 goroutine 之间的逻辑应该独立，不依赖；

11.goroutine 和协程的不同之处在于，goroutine 支持并行，而协程通常不能，goroutine 通过 channel 进行通信，而协程通过 yield 和 resume 进行；

12.channel 类似 pipe ，goroutine 可以通过 channel 进行数据的传递来进行通信；

13.Go 语言的 channel 在任意时刻，只能有一个 goroutine 可以获取这个 channel 的数据，这是从设计上避免了 race condition；

14.channel 的定义形式 `var identifier chan datatype`；

15.一个 channel 只能传递一种类型的数据，但是通过指定为空接口，我们可以实现传递任意类型数据；

16.channel 是一种消息队列的模型，FIFO；

17.channel 是引用类型，只能通过 make 创建和分配内存；

18.发送数据到一个 channel `ch <- int1`， 从一个 channel 接收数据 `int2 = <- ch`，可以看到 channel 是通过 `<-` 这个操作符来进行数据的传递的；

19.默认情况下，通过 channel 的通信是异步和不缓存的，如果没有 receiver 去接收值，send 操作不会完成，如果另外一端没有 sender 发送数据， receive 操作不会完成，所以 send/receive 操作是会堵塞直至另外一端准备好；

20.可以通过 `make(chan datatype, buf)` 来创建带有 buf 个位置的 channel ，也就是可以往这个 channel 中发送 buf 个同类型的数据；

21.对带有 buf 的 channel ，send 操作不会阻塞直至 channel 满了，receive 操作不会阻塞直至这个 channel 空了；

22.当 channel 的 send/receive 操作阻塞的时候，会阻塞整个 goroutine 的执行，对于 main 函数同样也会阻塞；

23.`var send_only chan<-int` 定义的 send_only 只能用来发送数据， `var recv_only <-chan int` 定义的 recv_only 只能用来接收数据；

24.默认创建的 channel 既可以用来接收数据也可以用来发送数据，是双向的；


