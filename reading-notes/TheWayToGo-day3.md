## TheWayToGo-day3

今天阅读了 8-10 章节的内容，这三章主要包括了 Map 类型，Go 语言的包以及用户自定义的类型 struct 和方法 method。其中我觉得最有意思的是 Go 语言中的 struct ，通过用户自定义类型 struct 和 方法 method，即使 Go 语言没有显式地提供面向对象的支持，也可以实现类似面向对象的设计，甚至可以模拟多重继承，而且更加清晰。下面是这几章的要点总结。

### 第八章：Maps

Go 中的 Map 类型是一种无序的集合类型，其单个元素为一对元素，其中一个为 key ，另外一个为 value。很明显，这种类型也就是其他语言中常见的联合数组或者哈希类型，其底层的实现为哈希表，通过 key 可以很快地查找到对应的 value。下面是本章的要点：

1.map 是引用类型，定义的形式为：`var map1 map[keytype]valuetype`；

2.虽然在 map 中查找一个 key 速度比线性搜索要快很多，但是还是比数组和 slice 类型通过索引直接定位要慢很多；

3.如果尝试通过 map1[key1] 从 map1 中获取不存的 key1 的值，将会返回一个 valuetype 的零值；

4.`val, isPresent := map1[key1]`，实际上返回两个值，第一个值是如果存在该 key1 则返回对应值，否则为零值，第二个值如果存在 key1 则为 true，否则为 false；

5.对一个 map 排序，可以通过对其 key 进行排序；

6.`range map1`将会得到这个 map 的 key 和 value 对；

### 第九章：Packages

本章内容是关于 Go 语言的包。包是 Go 组织代码的一种方式，其他人写的代码库可以通过包的形式发布，提供给其他人使用。而 Go 语言官方也有大量的标准库包提供给语言使用者，这些标准库包括方方面面的内容从 io 到 字符串处理，从数学到 http 协议解析。本章介绍了一些来自标准库的包，比如正则表达式的包 regexp 和处理多同步的 sync 包。对于个人的包，可以通过 godoc 工具来提取注释生成文档页面，这一点是非常方便的。本章还介绍了组织包源码的方式，比如同一个包的源文件最好放在和包名一致的同一个目录下。此外，还可以通过 go get 和 go install 工具安装来自其他版本库网站上面的代码，比如 github 上面的 go 项目也可以直接获取安装。

### 第十章：Structs and Methods

这章讲的是用户自定义的数据类型 struct 和可以绑定到类型的特殊类似函数的 method。通过 struct 类型，可以根据系统的实际需要定义新的类型。struct 是一个组合类型，可以包含不同类型的数据。下面是本章的要点：

1.sturct 的内容为 field ，一个 field 包含一个名称和一个类型，此外在一个 struct 内 field 的名称必须是唯一的；

2.定义的形式`type identifier struct { field1 type1 }`；

3.struct 可以通过 new 函数来分配内存，返回一个指向该分配的内存地址，也就是指针；

4.也可以通过 `strutname.field = value` 的方式来修改一个 struct 变量的字段的值；

5.对于 struct 变量来说，不管是值变量还是指针变量，都是通过 `.` 操作符来访问字段，如果是 struct 指针，会自动解引用；

6.一个 struct 变量在内存中的布局是连续地按照定义的顺序保存各个 field 的值；

7.make 函数不能用来创建一个 struct 类型的变量；

8.struct 类型的字段除了有名称和类型之外，也允许定义一个 tag 来对这个字段进行说明，这个 tag 只能通过 reflect 的函数获取到，通常情况下的编程无法使用；

9.带 tag 的 struct 定义：`field type "tag"`；

10.如果一个 struct 的某个字段只有类型而没有名称，则这个字段被称为匿名字段，可以通过 structname.type 访问；

11.一个 struct 内，同一种类型的匿名字段只能有一个，这是明显的，如果有多个同类型匿名字段很明显无法识别访问的是哪个；

12.一个 struct 同样也可以作为另外一个 struct 类型的匿名字段，这个 struct 被称为内嵌到另外一个 struct 类型；

13.如果内嵌的 struct 和外层的 struct 有同名的字段，则外层会屏蔽内层的该字段；

14.如果同一个层内有两个同名的字段，则会发生错误；

15.外层 struct 的变量可以直接通过点操作符引用内嵌的 struct 的字段，也就是内嵌的 struct 的字段可以看作为被外层的 struct 继承了；

16.Go 语言的方法 (method) 是一种作用在特定类型的函数，这个特定类型被称为 receiver；

17.method 不支持函数重载，但是对应不同的 receiver 可以有同名的 method；

18.method 和其对应的类型需要在同一个包定义；

19.method 的定义形式：`func (recv receiver_type) methodname(parameters) (return_values) { ... }`；

20.某个类型变量的 method 的调用方式：`recv.Method()`；

21.定义 method 的时候可以将 receiver 类型的指针作为参数，这样可以在 method 内修改该变量的值；

22.无论是指针 method 还是值 method 都可以被该类型的指针或者值变量调用；

23.定义一个 struct 的时候，如果它的字段的名称首字母不是大写的话，同样对外部包来说也是不可见的；

24.当一个 struct 内嵌到另外一个 struct 的时候，外层的 struct 同样也继承了这个内嵌的 struct 类型的可见 method；

25.同样地，外层 struct 的同名 method 会屏蔽内嵌 struct 的同名 method 的访问；

26.Go 语言的 struct 可以内嵌多个不同类型的 struct 类型的匿名字段，这样可以实现了类似多重继承的效果，而且更加清晰轻便；

27.有些类型的方法是通用的方法，比如 `String()` 方法会在输出时用 `%v` 格式控制的时候调用，此外还有 `Open()` `Read()` `Write()` 等通用方法；